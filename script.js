var obj = {};
function validateForm() {
    let email = document.getElementById("mail").value;
    let password = document.getElementById("pass").value;
    if (email.trim() == "" || password.trim() == "") {
        alert("Email or Password can not be empty");
        return false;
    }
    else {
        obj.Email = email;
        obj.Password = password;
        return true;
    }
}
function validateRadio() {
    let radio = document.getElementByName('role');
    if (radio[0].checked)
        obj.Role = radio[0].value;
    else if (radio[1].checked)
        obj.Role = radio[1].value;

    if (radio[0].checked || radio[1].checked)
        return true;
    else {
        alert("Select a role");
        return false;
    }

}

function validateCheckbox() {
    let c = 0;
    let flag = false;
    if (document.getElementById("perm1").checked) { obj.Permission1 = "YES"; c++; }
    if (document.getElementById("perm2").checked) { obj.Permission2 = "YES";; c++; }
    if (document.getElementById("perm3").checked) { obj.Permission3 = "YES"; c++; }
    if (document.getElementById("perm4").checked) { obj.Permission4 = "YES"; c++; }
    if (c >= 2) {
        flag = true;
        return true;
    }
    if (!flag) {
        alert("Select atleast two permissions");
        return false;
    }
}
function validatePassword() {
    let pass = document.getElementById("pass").value;
    if (pass.length < 8) {
        alert("Password should be of atleast 8 characters");
        return false;
    }
    if (pass.search(/[0-9]/) == -1) {
        alert("Password must contain atleast 1 digit");
        return false;
    }
    if (pass.search(/[a-z]/) == -1) {
        alert("Password must contain atleast 1 small letter");
        return false;
    }
    if (pass.search(/[A-Z]/) == -1) {
        alert("Password must contain atleast 1 capital letter");
        return false;
    }
    return true;
}
